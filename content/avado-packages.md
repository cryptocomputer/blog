


---
title: Avado packages overview
---


This page list all packages currently available on the [Avado Dappstore](https://ava.do/#dappstore)

Click on the package name to get more information  

||Name|Version|Upstream version|
| :---: | :---: | :---: | :---: |
|{{< figure src="/packages/turbogeth.avado.dnp.dappnode.eth.png" alt="Ethereum Node (TurboGeth)" width="100" >}}|[Ethereum Node (TurboGeth)](/packages/turbogeth.avado.dnp.dappnode.eth)|0.0.1|v2020.10.01|
|{{< figure src="/packages/ryo-client.avado.dnp.dappnode.eth.png" alt="RYO client" width="100" >}}|[RYO client](/packages/ryo-client.avado.dnp.dappnode.eth)|0.0.18||
|{{< figure src="/packages/ropsten.dnp.dappnode.eth.png" alt="Ropsten Testnet" width="100" >}}|[Ropsten Testnet](/packages/ropsten.dnp.dappnode.eth)|0.3.2||
|{{< figure src="/packages/rinkeby.dnp.dappnode.eth.png" alt="Rinkeby Testnet" width="100" >}}|[Rinkeby Testnet](/packages/rinkeby.dnp.dappnode.eth)|0.4.6||
|{{< figure src="/packages/prysmvalidator.avado.dnp.dappnode.eth.png" alt="Prysm ETH2.0 validator client (Medalla)" width="100" >}}|[Prysm ETH2.0 validator client (Medalla)](/packages/prysmvalidator.avado.dnp.dappnode.eth)|0.2.17|v1.0.0-alpha.23|
|{{< figure src="/packages/prysm-beacon-chain.avado.dnp.dappnode.eth.png" alt="Prysm ETH2.0 beacon chain (Medalla)" width="100" >}}|[Prysm ETH2.0 beacon chain (Medalla)](/packages/prysm-beacon-chain.avado.dnp.dappnode.eth)|0.3.17|v1.0.0-beta.0.rc|
|{{< figure src="/packages/prysm-beacon-chain-mainnet.avado.dnp.dappnode.eth.png" alt="Prysm ETH2.0 beacon chain" width="100" >}}|[Prysm ETH2.0 beacon chain](/packages/prysm-beacon-chain-mainnet.avado.dnp.dappnode.eth)|0.0.15|v1.0.5|
|{{< figure src="/packages/polkadotnode.avado.dnp.dappnode.eth.png" alt="Polkadot archive node" width="100" >}}|[Polkadot archive node](/packages/polkadotnode.avado.dnp.dappnode.eth)|0.0.2|v0.8.27|
|{{< figure src="/packages/mysteriumnode.avado.dnp.dappnode.eth.png" alt="Mysterium VPN Node" width="100" >}}|[Mysterium VPN Node](/packages/mysteriumnode.avado.dnp.dappnode.eth)|0.0.40|0.38.1|
|{{< figure src="/packages/mycrypto.avado.dnp.dappnode.eth.png" alt="MyCrypto Wallet" width="100" >}}|[MyCrypto Wallet](/packages/mycrypto.avado.dnp.dappnode.eth)|0.0.6|2.2.2|
|{{< figure src="/packages/monero.dnp.dappnode.eth.png" alt="Monero Node" width="100" >}}|[Monero Node](/packages/monero.dnp.dappnode.eth)|0.2.3|v0.17.1.5|
|{{< figure src="/packages/lightning-network.dnp.dappnode.eth.png" alt="Lightning Network" width="100" >}}|[Lightning Network](/packages/lightning-network.dnp.dappnode.eth)|0.2.0||
|{{< figure src="/packages/kusama.avado.dnp.dappnode.eth.png" alt="KUSAMA archive node" width="100" >}}|[KUSAMA archive node](/packages/kusama.avado.dnp.dappnode.eth)|0.0.12|v0.8.26-1|
|{{< figure src="/packages/kovan.avado.dnp.dappnode.eth.png" alt="Kovan Testnet (Ethereum)" width="100" >}}|[Kovan Testnet (Ethereum)](/packages/kovan.avado.dnp.dappnode.eth)|10.0.4|openethereum (latest)|
|{{< figure src="/packages/hopr.avado.dnp.dappnode.eth.png" alt="HOPR client" width="100" >}}|[HOPR client](/packages/hopr.avado.dnp.dappnode.eth)|1.62.15||
|{{< figure src="/packages/goerli-geth.avado.dnp.dappnode.eth.png" alt="Goerli Testnet (Ethereum)" width="100" >}}|[Goerli Testnet (Ethereum)](/packages/goerli-geth.avado.dnp.dappnode.eth)|10.4.6|1.9.24|
|{{< figure src="/packages/ethforward.dnp.dappnode.eth.png" alt="ENS resolver" width="100" >}}|[ENS resolver](/packages/ethforward.dnp.dappnode.eth)|10.0.1||
|{{< figure src="/packages/ethchain-geth.public.dappnode.eth.png" alt="Ethereum Node (Geth)" width="100" >}}|[Ethereum Node (Geth)](/packages/ethchain-geth.public.dappnode.eth)|10.0.13|v1.9.24|
|{{< figure src="/packages/eth2validator.avado.dnp.dappnode.eth.png" alt="Prysm ETH2.0 validator" width="100" >}}|[Prysm ETH2.0 validator](/packages/eth2validator.avado.dnp.dappnode.eth)|0.0.12|v1.0.5|
|{{< figure src="/packages/eth2keygen.avado.dnp.dappnode.eth.png" alt="ETH2.0 Key Generator" width="100" >}}|[ETH2.0 Key Generator](/packages/eth2keygen.avado.dnp.dappnode.eth)|0.0.16||
|{{< figure src="/packages/dappmanager.dnp.dappnode.eth.png" alt="The AVADO DappManger" width="100" >}}|[The AVADO DappManger](/packages/dappmanager.dnp.dappnode.eth)|10.0.18||
|{{< figure src="/packages/bitcoin.avado.dnp.dappnode.eth.png" alt="Bitcoin" width="100" >}}|[Bitcoin](/packages/bitcoin.avado.dnp.dappnode.eth)|10.0.5|v0.21.0|
|{{< figure src="/packages/bind.dnp.dappnode.eth.png" alt="AVADO DNS resolver" width="100" >}}|[AVADO DNS resolver](/packages/bind.dnp.dappnode.eth)|10.0.4||
|{{< figure src="/packages/bee.avado.dnp.dappnode.eth.png" alt="Swarm node (BEE)" width="100" >}}|[Swarm node (BEE)](/packages/bee.avado.dnp.dappnode.eth)|0.0.4|v0.3.1|
|{{< figure src="/packages/avalanchego.avado.dnp.dappnode.eth.png" alt="Avalanche node" width="100" >}}|[Avalanche node](/packages/avalanchego.avado.dnp.dappnode.eth)|0.1.33|v1.1.1|
|{{< figure src="/packages/avado-tornadocashrelayerv2.public.dappnode.eth.png" alt="Tornado Cash Relayer V2" width="100" >}}|[Tornado Cash Relayer V2](/packages/avado-tornadocashrelayerv2.public.dappnode.eth)|0.0.1|4.0.0|
|{{< figure src="/packages/avado-dnp-nethermind.public.dappnode.eth.png" alt="Ethereum node (Nethermind)" width="100" >}}|[Ethereum node (Nethermind)](/packages/avado-dnp-nethermind.public.dappnode.eth)|0.0.22|1.9.41|
|{{< figure src="/packages/admin.dnp.dappnode.eth.png" alt="AVADO Admin UI" width="100" >}}|[AVADO Admin UI](/packages/admin.dnp.dappnode.eth)|10.0.25||
