---
title: "Use your Avado Geth chain in MetaMask"
date: 2021-01-21
lastmod: 2021-01-23
tags: ["metamask", "avado"]
categories: ["avado"]
featuredImage: metamask.png
hiddenFromHomePage: false
---

By default, Metamask uses infura to connect to the Ethereum blockchain. This is convenient, but also a centralized depedency.
When you run a Avado box with the [Geth]({{<relref "packages/ethchain-geth.public.dappnode.eth.md">}}) package, you are running a full node. So what better connection to the Ethereum blockchain can you ask for? You can be sure of the content, you have decentralized access and you get better privacy as a bonus.

In this article, I'll explain how to configure MetaMask to use your own Avado Ethereum node.

<!--more-->

## Configuration

1. Connect to your Avado node through VPN
2. Open MetaMask (expand view)
   ![MetaMask](expand.png)
3. Go to **Settings**  
   ![MetaMask settings](settings.png)
4. Select **Networks**
   <!-- ![MetaMask network settings](network.png) -->
5. Click **Add network** and enter these values:
    * **Network name:** `AVADO`
    * **New RPC URL:** `http://my.ethchain-geth.public.dappnode.eth:8545`
    * **Chain ID:** `1`
    * **Currency Symbol (optional):** `ETH`
    * **Block Explorer URL (optional):** `https://etherscan.io`  
    ![MetaMask network settings for Avado](avado.png)
6. Click **Save**

Congratulations, you are now using your own Avado node as Ethereum network provider.

## Using Avado without a VPN connection: Avado RYO

In Step 5 use following values:
* **Network name:** `AVADO RYO`
* **New RPC URL:** `https://mainnet.eth.cloud.ava.do`
* **Chain ID:** `1`
* **Currency Symbol (optional):** `ETH`
* **Block Explorer URL (optional):** `https://etherscan.io`  

## Troubleshooting

If you get the message **"Oops! Something went wrong."**, make sure you have an active connection to your Avado VPN.
![Connecting](connecting.png)![Connecting failed](connecting_failed.png)

If you get the message **Could not fetch chain ID. Is your RPC URL correct?**, make sure you have an active connection to your Avado VPN.  
![error](error.png)