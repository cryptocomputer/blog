


---
title: Tornado Cash Relayer V2
tags: [package]
featuredImage: /packages/avado-tornadocashrelayerv2.public.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Tornado Cash Relayer V2](/packages/avado-tornadocashrelayerv2.public.dappnode.eth.png)
# Description


AVADO Tornadocash relayer V2

MIT
# Default options


WEB3PROVIDERS=ethchain-geth.public.dappnode.eth,ethchain.dnp.dappnode.eth,avado-dnp-nethermind.public.dappnode.eth