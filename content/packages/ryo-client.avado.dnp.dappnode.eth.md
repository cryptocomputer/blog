


---
title: RYO client
tags: [package]
featuredImage: /packages/ryo-client.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![RYO client](/packages/ryo-client.avado.dnp.dappnode.eth.png)
# Description


The RYO client package allows you to share services of your AVADO box in clusters to other users to provide high-availability in decentralized clouds. You can share your Ethereum node, your IPFS node or participate in the TornadoCash relayer pool to earn a profit !

(c)
# Default options


ACLOUD_ENDPOINT=https://acloud.ava.do