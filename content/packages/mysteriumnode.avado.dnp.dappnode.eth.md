


---
title: Mysterium VPN Node
tags: [package]
featuredImage: /packages/mysteriumnode.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Mysterium VPN Node](/packages/mysteriumnode.avado.dnp.dappnode.eth.png)
# Description


The Mysterium package turns your AVADO box into a VPN endpoint. VPN traffic is provided by the Mysterium Network. You can earn crypto by renting out your spare internet bandwidth at home.

(c)