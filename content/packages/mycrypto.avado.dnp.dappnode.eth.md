


---
title: MyCrypto Wallet
tags: [package]
featuredImage: /packages/mycrypto.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![MyCrypto Wallet](/packages/mycrypto.avado.dnp.dappnode.eth.png)
# Description


MyCrypto is a crypto asset manager. You can run it from your AVADO for maximum privacy. Connect it to your own ETH node to enjoy full privacy.

(c)