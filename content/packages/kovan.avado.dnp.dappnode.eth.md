


---
title: Kovan Testnet (Ethereum)
tags: [parity, kovan, ethereum, testnet, package]
featuredImage: /packages/kovan.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Kovan Testnet (Ethereum)](/packages/kovan.avado.dnp.dappnode.eth.png)
# Description


This package provides another Ethereum testnet that you can sync. Kovan is an Ethereum testnet using Parity's Proof of Authority consensus engine. A testnet can be useful when developing dapps. If you’re a developer - this package can be very useful for you.

GPL-3.0
# Default options


EXTRA_OPTS=--chain=kovan --port=30323 --jsonrpc-port 8545 --jsonrpc-interface all --jsonrpc-hosts all --jsonrpc-cors all --ws-interface all --ws-port 8546 --ws-origins all --ws-hosts all