


---
title: Ethereum node (Nethermind)
tags: [package]
featuredImage: /packages/avado-dnp-nethermind.public.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Ethereum node (Nethermind)](/packages/avado-dnp-nethermind.public.dappnode.eth.png)
# Description


This package installs an Ethereum full node on your AVADO. It installs the latest Nethermind client and makes its RPC ports available on the network of your AVADO so that you can interact with the blockchain. Please forward port 30306/tcp on your router to your AVADO box for best results.

(C)