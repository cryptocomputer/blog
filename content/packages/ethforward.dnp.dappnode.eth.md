


---
title: ENS resolver
tags: [ETHFORWARD, ENS, package]
featuredImage: /packages/ethforward.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![ENS resolver](/packages/ethforward.dnp.dappnode.eth.png)
# Description


This is a package that allows you to browse .eth sites from your AVADO. These websites are decentralized and hosted on IPFS or other decentralised file storage. By browsing these websites - your AVADO also becomes a host of their data, helping to achieve true decentralisation of the web.

GPL-3.0
# Default options


PIN_CONTENT_ON_VISIT=

WEB3PROVIDERS=ethchain-geth.public.dappnode.eth,ethchain.dnp.dappnode.eth,avado-dnp-nethermind.public.dappnode.eth