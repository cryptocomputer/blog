


---
title: Ethereum Node (TurboGeth)
tags: [package]
featuredImage: /packages/turbogeth.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Ethereum Node (TurboGeth)](/packages/turbogeth.avado.dnp.dappnode.eth.png)
# Description


Alternative implementation of Ethereum (Eth 1.x), derived from go-ethereum. The main difference between turbo-geth and go-ethereum is in the way the database is used to store and access the Ethereum state and its history. This change has a profound effect on many parts of the code. Currently, turbo-geth only supports full sync from genesis block, but it will support snapshot sync which is more efficient than fast sync and more secure than warp sync. Turbo-geth is also used as a platform for the research into Stateless Ethereum.

GLP-3.0