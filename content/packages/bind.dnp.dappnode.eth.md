


---
title: AVADO DNS resolver
tags: [DAppNodeCore, bind, DNS, package]
featuredImage: /packages/bind.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![AVADO DNS resolver](/packages/bind.dnp.dappnode.eth.png)
# Description


AVADO Dappnode package responsible for providing DNS resolution

GPL-3.0