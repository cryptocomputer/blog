


---
title: Prysm ETH2.0 validator client (Medalla)
tags: [package]
featuredImage: /packages/prysmvalidator.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Prysm ETH2.0 validator client (Medalla)](/packages/prysmvalidator.avado.dnp.dappnode.eth.png)
# Description


This is the Medalla ETH2.0 testnet validator client which manages a staking user's private/public keys, connects to a beacon node, and acts as a validator in Ethereum 2.0 by staking 32 ETH and participating in proof of stake consensus by proposing and voting on blocks. (Testnet tokens here have no value)

GPL-3.0
# Default options


VERBOSITY=info

EXTRA_OPTS