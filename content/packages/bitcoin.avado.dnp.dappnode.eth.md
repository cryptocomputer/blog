


---
title: Bitcoin
tags: [package]
featuredImage: /packages/bitcoin.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Bitcoin](/packages/bitcoin.avado.dnp.dappnode.eth.png)
# Description


“Don’t trust - verify!” Bitcoin is the mother of all cryptocurrencies. Using this package you can run a bitcoin full node (livenet or testnet) at home and participate in verification of blocks, and keeping a copy of the full state of the Bitcoin network. Be a proud supporter of decentralisation!

GPL-3.0
# Default options


BTC_RPCUSER=avado

BTC_RPCPASSWORD=avado

BTC_TXINDEX=1

BTC_PRUNE=0

BTC_TESTNET=0