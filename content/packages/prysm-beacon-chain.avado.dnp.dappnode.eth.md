


---
title: Prysm ETH2.0 beacon chain (Medalla)
tags: [package]
featuredImage: /packages/prysm-beacon-chain.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Prysm ETH2.0 beacon chain (Medalla)](/packages/prysm-beacon-chain.avado.dnp.dappnode.eth.png)
# Description


ETH2 Mainnet beacon chain

(c)
# Default options


EXTRA_OPTS=--medalla --http-web3provider=http://my.goerli-geth.avado.dnp.dappnode.eth:8545 --deposit-contract=0x07b39F4fDE4A38bACe212b546dAc87C58DfE3fDC