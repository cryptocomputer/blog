


---
title: Prysm ETH2.0 beacon chain
tags: [package]
featuredImage: /packages/prysm-beacon-chain-mainnet.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Prysm ETH2.0 beacon chain](/packages/prysm-beacon-chain-mainnet.avado.dnp.dappnode.eth.png)
# Description


ETH2 Mainnet beacon chain

(c)
# Default options


EXTRA_OPTS=--http-web3provider=http://my.ethchain-geth.public.dappnode.eth:8545 --grpc-gateway-corsdomain=http://prysm-beacon-chain-mainnet.avadopackage.com,http://eth2validator.avadopackage.com