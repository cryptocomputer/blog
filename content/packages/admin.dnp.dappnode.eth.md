


---
title: AVADO Admin UI
tags: [DAppNodeCore, UI, Admin, package]
featuredImage: /packages/admin.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![AVADO Admin UI](/packages/admin.dnp.dappnode.eth.png)
# Description


This package provides the Administration UI of your AVADO. It is a core package and should not be removed.

GPL-3.0