


---
title: HOPR client
tags: [package]
featuredImage: /packages/hopr.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![HOPR client](/packages/hopr.avado.dnp.dappnode.eth.png)
# Description


The HOPR protocol ensures everyone has control of their privacy, data, and identity. By running a HOPR Node, you can obtain HOPR tokens by relaying data and connect to the HOPR Network.

(C)
# Default options


DEBUG=hopr*