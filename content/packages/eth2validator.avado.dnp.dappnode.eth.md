


---
title: Prysm ETH2.0 validator
tags: [package]
featuredImage: /packages/eth2validator.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Prysm ETH2.0 validator](/packages/eth2validator.avado.dnp.dappnode.eth.png)
# Description


the ETH2.0 validator for AVADO

(c)
# Default options


EXTRA_OPTS=--graffiti=AVADO --beacon-rpc-provider=my.prysm-beacon-chain-mainnet.avado.dnp.dappnode.eth:4000 --beacon-rpc-gateway-provider=my.prysm-beacon-chain-mainnet.avado.dnp.dappnode.eth:3500