


---
title: Avalanche node
tags: [package]
featuredImage: /packages/avalanchego.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Avalanche node](/packages/avalanchego.avado.dnp.dappnode.eth.png)
# Description


This is the Avalanche Mainnet node. Avalanche is an open-source platform for launching highly decentralized applications, new financial primitives, and new interoperable blockchains.

(C)
# Default options


EXTRA_OPTS