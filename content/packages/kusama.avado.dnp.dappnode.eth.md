


---
title: KUSAMA archive node
tags: [polkadot, package]
featuredImage: /packages/kusama.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![KUSAMA archive node](/packages/kusama.avado.dnp.dappnode.eth.png)
# Description


This package will run the latest Kusama node on your AVADO box. Kusama is the Polkadot testnet and serves as a proving ground, allowing teams and developers to build and deploy a parachain or try out Polkadot’s governance, staking, nomination and validation functionality in a real environment.

MIT
# Default options


EXTRA_OPTS=--rpc-external --rpc-cors all --port 30334 --ws-external --pruning=archive --chain kusama --name AVADO