


---
title: The AVADO DappManger
tags: [Manager, Installer, package]
featuredImage: /packages/dappmanager.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![The AVADO DappManger](/packages/dappmanager.dnp.dappnode.eth.png)
# Description


The DappManager takes care of installing and uninstalling packages from the DappStore on your AVADO. It is a core package and should not be removed.

GPL-3.0