


---
title: Monero Node
tags: [monero, package]
featuredImage: /packages/monero.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Monero Node](/packages/monero.dnp.dappnode.eth.png)
# Description


The Monero daemon monerod keeps your computer synced up with the Monero network. It downloads and validates the blockchain from the p2p network. `monerod` is entirely decoupled from your wallet. `monerod` does not access your private keys - it is not aware of your transactions and balance. Read the [monerod reference](https://monerodocs.org/interacting/monerod-reference/) to know all you can do with this DAppNode Package.

GPL-3.0
# Default options


EXTRA_OPTS=