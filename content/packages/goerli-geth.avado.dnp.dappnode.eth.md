


---
title: Goerli Testnet (Ethereum)
tags: [package]
featuredImage: /packages/goerli-geth.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Goerli Testnet (Ethereum)](/packages/goerli-geth.avado.dnp.dappnode.eth.png)
# Description


This package provides a Geth Ethereum client that is configured to sync the Görli Testnet. The Görli Testnet is the first proof-of-authority cross-client testnet. A testnet can be useful when developing dapps. If you’re a developer - this package can be very useful for you.

GPL-3.0
# Default options


EXTRA_OPTS=--http.api eth,net,web3,txpool