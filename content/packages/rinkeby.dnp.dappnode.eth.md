


---
title: Rinkeby Testnet
tags: [geth, rinkeby, ethereum, testnet, go-ethereum, package]
featuredImage: /packages/rinkeby.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Rinkeby Testnet](/packages/rinkeby.dnp.dappnode.eth.png)
# Description


This package provides an Ethereum testnet node. Rinkeby is an Ethereum testnet and can be useful when developing dapps. If you’re a developer - this package is for you. You can use the JSON/RPC endpoint of this testnet node to test your dapps.

GPL-3.0