


---
title: Lightning Network
tags: [bitcoin, btc, lightning network, lnd, package]
featuredImage: /packages/lightning-network.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Lightning Network](/packages/lightning-network.dnp.dappnode.eth.png)
# Description


The Lightning Network is a decentralized system for instant, high-volume micropayments on Bitcoin that remove the risk of delegating custody of funds to trusted third parties. This package will run the Lightning client - and should be installed alongside with your bitcoin node.

GPL-3.0