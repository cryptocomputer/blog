


---
title: Ethereum Node (Geth)
tags: [package]
featuredImage: /packages/ethchain-geth.public.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Ethereum Node (Geth)](/packages/ethchain-geth.public.dappnode.eth.png)
# Description


This package installs an Ethereum full node on your AVADO. It installs the latest Go-Ethereum (“geth”) client and makes its RPC ports available on the network of your AVADO so that you can interact with the blockchain. This is the default and recommended Ethereum client.

(C)
# Default options


EXTRA_OPTS=--rpcapi eth,net,web3,txpool