


---
title: Polkadot archive node
tags: [package]
featuredImage: /packages/polkadotnode.avado.dnp.dappnode.eth.png
date: 2021-01-21
lastmod: 2021-01-23
hiddenFromHomePage: true
---
  
![Polkadot archive node](/packages/polkadotnode.avado.dnp.dappnode.eth.png)
# Description


Polkadot Full Node - which can be used for DOT staking

(c)
# Default options


EXTRA_OPTS=--pruning=archive --validator --rpc-cors=*