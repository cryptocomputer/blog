import requests
from pprint import pprint
import json
import os.path
import shutil
import datetime

from mdutils.mdutils import MdUtils
# https://mdutils.readthedocs.io/en/latest/mdutils.html
# https://github.com/didix21/mdutils

ipfs_cache="python/cache/"

def getIpfs(hash):
    cache_path=ipfs_cache+hash
    if not os.path.isfile(cache_path):
        url='https://ipfs.cloud.ava.do/ipfs/'+str(hash)
        req=requests.get(url=url)
        with open(cache_path, 'wb') as f:
            for chunk in req.iter_content():
                f.write(chunk)
    return cache_path

def getJson(path):
    with open(path, 'r') as f:
        return json.load(f)


ipfs_hash = json.loads(requests.get(url='https://bo.ava.do/value/store').json()).get("hash")



data = getJson(getIpfs(ipfs_hash))
# pprint(ipfs_hash)

#test preview versions:
if False:
    test = data
    while test.get("previousversion"):
        pprint(test.get("previousversion"))
        test = getJson(getIpfs(test.get("previousversion")))

# Create table
mdFile = MdUtils(file_name='content/avado-packages')
mdFile.write('---\n')
mdFile.write('title: Avado packages overview\n')
mdFile.write('---\n')

mdFile.new_paragraph("This page list all packages currently available on the [Avado Dappstore](https://ava.do/#dappstore)")

mdFile.new_paragraph("Click on the package name to get more information")

package_table = ["", "Name", "Version", "Upstream version"]
for package in data.get("packages"):
    manifest = package.get("manifest")
    name = manifest.get("name")
    title = manifest.get("title")
    description = manifest.get("description")
    keywords = manifest.get("keywords",[])
    today = datetime.date.today()

    avatar = "/packages/" + name + ".png"
    shutil.copyfile(getIpfs(manifest.get("avatar").removeprefix("/ipfs/")), "static" + avatar)

    # md_avatar = '!['+title+'](/packages/'+name+'.png)'
    # md_avatar = '<img src="/packages/'+name+'.png" alt="'+title+'" width="200"/>'
    md_avatar = '{{< figure src="/packages/'+name+'.png" alt="'+title+'" width="100" >}}'
    md_title = '['+title+'](/packages/'+name+')'
    package_table.extend([md_avatar,md_title, manifest.get("version"), manifest.get("upstream","")])

    packageFile = MdUtils(file_name="content/packages/"+name)
    packageFile.write('---\n')
    packageFile.write('title: '+title+'\n')
    packageFile.write('tags: ['+ ", ".join(keywords + ["package"]) +']\n')
    packageFile.write('featuredImage: ' + avatar + "\n")
    packageFile.write('date: 2021-01-21\n')
    packageFile.write('lastmod: '+today.strftime('%Y-%m-%d')+'\n')
    packageFile.write('hiddenFromHomePage: true\n')
    packageFile.write('---\n')
    packageFile.new_line(packageFile.new_inline_image(title, avatar))
    packageFile.new_header(level=1, title='Description')
    packageFile.new_paragraph(description)
    packageFile.new_paragraph(manifest.get("license"))
    options = manifest.get('image',[]).get('environment',[])
    if options:
        packageFile.new_header(level=1, title='Default options')
        for option in manifest.get('image',[]).get('environment',[]):
            packageFile.new_paragraph(option)

    packageFile.create_md_file()



mdFile.new_line()
mdFile.new_table(columns=4, rows=(len(data.get("packages"))+1), text=package_table, text_align='center')

# pprint(data)
mdFile.create_md_file()